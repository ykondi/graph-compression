Graph Compression
=================

Authors
-------
* Anisha Nazareth
* Yashvanth K

Description
-----------

The purpose of this project is to implement graph compression in OCaml. The
algorithm used here is LZW (Lempel–Ziv–Welch) for specific graphs with
non-unique instances of pre-defined node classes.
