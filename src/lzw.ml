open Graph
open Stringraph


type dictEntry = Entry of (string * string);;

(* Helper functions *)
let chr_inc c = Char.chr ((Char.code c)+1);;
let strchr c = Char.escaped c ;;

let rec construct_dict nodeClasses index =
	match nodeClasses with
	[] -> []
	| NodeClass(c)::rest -> Entry(strchr c, strchr index)::construct_dict rest (chr_inc index)

let rec last_index dict =
	match dict with Entry(_,i)::[] -> i.[0]
	| _::r -> last_index r
	| [] -> raise Not_found

let rec get_dict_index str dict =
	match dict with
	[] -> "-"
	| Entry(s,i)::_ when s = str -> i
	| _::rest -> get_dict_index str rest;;

let rec get_dict_str index dict =
	match dict with
	[] -> "-"
	| Entry(s,i)::_ when i = index -> s
	| _::rest -> get_dict_str index rest;;

let rm_first str = (String.sub str 1 (String.length str - 1))

(*
  Generate dictionary-based alphabet replacements for a stringified graph
*)
let compress strs nodeClasses =
	let rec compress_inner strs dict d_index prev =
		match strs with
		[] -> get_dict_index prev dict
		| s::rest when s.[0] < 'A' ->
			(* Reached a node-break *)
			(get_dict_index prev dict)^s^compress_inner rest dict d_index ""
		| s::rest -> (
			     let entry = prev^s
			     in
			     let indx = get_dict_index entry dict
			     and
			     outp = get_dict_index prev dict
			     in
			     match indx with
			     "-" -> outp^compress_inner rest (Entry(entry,strchr d_index)::dict) (chr_inc d_index) s
			     | _ -> compress_inner rest dict d_index entry
			     )
	and start_dict = construct_dict nodeClasses 'a'
	in
	rm_first (compress_inner strs start_dict (chr_inc (last_index start_dict)) "");;


(*
  Decompress a given sequence of dictionary entries
*)
let decompress str nodeClasses =
	let rec get_number str =
		(* Extract number from head of string *)
		match str with
		"" -> ""
		| s when s.[0] >= 'A'-> ""
		| s -> (String.sub s 0 1) ^ get_number (rm_first str)
	and explode str =
		match str with
		"" -> []
		| _ -> String.sub str 0 1 :: explode (rm_first str)
	in
	let rec decompress_inner str w dict d_indx =
		match str with
		"" -> []
		| s when s.[0] < 'A' -> (
			(* Reached a node-break *)
			let num = get_number str
			in
			let rest = (String.sub str (String.length num) (String.length str - (String.length num)))
			in 
			let first_ch = String.sub rest 0 1
			in
			explode num @ ((get_dict_str first_ch dict)::decompress_inner (rm_first rest) first_ch dict d_indx)
			)
		| _ -> (
			let s = String.sub str 0 1
			in
			let chunk = get_dict_str s dict
			in
			let next_call entry =
				let new_dict_entry = entry w chunk
				in
				let new_dict = Entry(new_dict_entry,strchr d_indx)::dict
				in
				let outp = get_dict_str s new_dict
				in
				explode outp @ decompress_inner (rm_first str) outp new_dict (chr_inc d_indx)
			in
			match chunk with
			"-" -> (* Special case when dictionary entry doesn't exist *)
				(
				let spl_entry w chunk = w ^ String.sub w 0 1
				in
				next_call spl_entry
				)
			| _ -> (
				let entry w chunk = w ^ String.sub chunk 0 1
				in
				next_call entry
				)
			)
	and
	start_dict = construct_dict nodeClasses 'a'
	in
	"a"::decompress_inner str "a" start_dict (chr_inc (last_index start_dict));;
