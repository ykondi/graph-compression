open Graph;;
open Stringraph;;
open Lzw;;

let a = NodeClass('a');;
let b = NodeClass('b');;

(* Create Nodes *)
let a1 = Node(0, a);;
let b1 = Node(1, b);;
let a2 = Node(2, a);;
let b2 = Node(3, b);;
let b3 = Node(4, b);;
let a3 = Node(5, a);;

(* Create adjacency list entries *)
let a1_adj = Adjacency(a1, [b1]);;
let b1_adj = Adjacency(b1, [a2; a3]);;
let a2_adj = Adjacency(a2, [b2; b3]);;
let b2_adj = Adjacency(b2, []);;
let a3_adj = Adjacency(a3, []);;
let b3_adj = Adjacency(b3, []);;

(* Wrap it as a Graph*)
let butn = Graph([a1_adj; b1_adj; a2_adj; b2_adj; a3_adj; b3_adj]);;

(* Encode this graph into one array *)
let enc_butn = encode butn;;

(* Convert this array into a string *)
let str_butn = stringify enc_butn;;

(* Compress the obtained string *)
let cmp_str_butn = compress str_butn [a;b];;

(*---------------------------------------------------------------------------*)

(* Decompress the above string*)
let dec_str_butn = decompress cmp_str_butn [a;b];;

(* Get back the array from the string *)
let grph_str = graphify dec_str_butn;;

(* Reconstruct the graph from its encoded form *)
let rec_butn = reconstruct grph_str;;
