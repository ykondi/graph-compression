open Graph;;
open Stringraph;;
open Lzw;;

let a = NodeClass('a');;
let b = NodeClass('b');;

(* Create Nodes *)
let a1 = Node(0, a);;
let b1 = Node(1, b);;
let a2 = Node(2, a);;
let b2 = Node(3, b);;
let a3 = Node(4, a);;
let b3 = Node(5, b);;
let a4 = Node(6, a);;
let b4 = Node(7, b);;
let a5 = Node(8, a);;
let b5 = Node(9, b);;
let a6 = Node(10, a);;
let b6 = Node(11, b);;
let a7 = Node(12, a);;
let b7 = Node(13, b);;
let a8 = Node(14, a);;
let b8 = Node(15, b);;
let a9 = Node(16, a);;
let b9 = Node(17, b);;

(* Create adjacency list entries *)
let a1_adj = Adjacency(a1, [b1]);;
let b1_adj = Adjacency(b1, [a2]);;
let a2_adj = Adjacency(a2, [b2]);;
let b2_adj = Adjacency(b2, [a3]);;
let a3_adj = Adjacency(a3, [b3]);;
let b3_adj = Adjacency(b3, [a4]);;
let a4_adj = Adjacency(a4, [b4]);;
let b4_adj = Adjacency(b4, [a5]);;
let a5_adj = Adjacency(a5, [b5]);;
let b5_adj = Adjacency(b5, [a6]);;
let a6_adj = Adjacency(a6, [b6]);;
let b6_adj = Adjacency(b6, [a7]);;
let a7_adj = Adjacency(a7, [b7]);;
let b7_adj = Adjacency(b7, [a8]);;
let a8_adj = Adjacency(a8, [b8]);;
let b8_adj = Adjacency(b8, [a9]);;
let a9_adj = Adjacency(a9, [b9]);;
let b9_adj = Adjacency(b9, []);;

(* Wrap it as a Graph*)
let chain = Graph([a1_adj; b1_adj; a2_adj; b2_adj; a3_adj; b3_adj;a4_adj; b4_adj; a5_adj; b5_adj; a6_adj; b6_adj;a7_adj; b7_adj; a8_adj; b8_adj; a9_adj; b9_adj;]);;

(* Encode this graph into one array *)
let enc_chain = encode chain;;

(* Convert this array into a string *)
let str_chain = stringify enc_chain;;

(* Compress the obtained string *)
let cmp_str_chain = compress str_chain [a;b];;

(*---------------------------------------------------------------------------*)

(* Decompress the above string*)
let dec_str_chain = decompress cmp_str_chain [a;b];;

(* Get back the array from the string *)
let grph_str = graphify dec_str_chain;;

(* Reconstruct the graph from its encoded form *)
let rec_chain = reconstruct grph_str;;



