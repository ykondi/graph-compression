(* Create two node classes, a and b *)
let a = Graph.NodeClass('a');;
let b = Graph.NodeClass('b');;

(* Create three node *)
let a1 = Graph.Node(0, a);;
let b1 = Graph.Node(1, b);;
let b2 = Graph.Node(2, b);;

(* Create edges between the nodes, and one composite edge *)
let a1_b1 = Graph.Basic(a1, b1);;
let b1_b2 = Graph.Basic(b1, b2);;
let a1_b1_b2 = Graph.Composite(a1, b1_b2);;

(* Create adjacency entries *)
let a1_adj = Graph.Adjacency(a1, [b1]);;
let b1_adj = Graph.Adjacency(b1, [b2]);;
let b2_adj = Graph.Adjacency(b2, []);;

(* Wrap the adjacency list as a graph *)
let graph = Graph.Graph([a1_adj;b1_adj;b2_adj]);;

(* Create an invalid graph *)
let invalid_graph = Graph.Graph([a1_adj;b2_adj]);;

(* Demonstrate how the graph can be encoded *)
let enc = Graph.encode graph;;

let graph_edge_list = Graph.EdgeList([a1_b1; b1_b2]);;
