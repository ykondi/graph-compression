open Graph

(*
  Convert a linearly encoded graph into a string
*)
let stringify sequence =
	let rec stringify_inner seq prev =
		match seq with
		[] -> []
		| nd::rest -> (
				let strchr c = Char.escaped c
				in
				match nd with
				Node(n,_) when n < prev ->
				(* Write node no. to indicate different branch *)
				(string_of_int n)::stringify_inner rest prev
				| Node(n,nc) -> (
						match nc with NodeClass(c) ->
						(strchr c)::stringify_inner rest n
						)
				)
	in
	stringify_inner sequence (-1);;


(*
  Reconstruct the linear graph encoding from its stringified form
*)
let graphify str =
	let rec getNode i nodes =
		match nodes with
		[] -> raise Not_found
		| Node(n,nc)::_ when n = i -> Node(n,nc)
		| _::rest -> getNode i rest
	in
	let rec graphify_inner str index nodes =
		match str with
		[] -> []
		| c::st when c.[0] > '9' -> let node = Node(index, NodeClass(c.[0]))
				in node::graphify_inner st (index+1) (nodes@[node])
		(* Skip numeric *)
		| n::st -> let i = int_of_string(n) in
			(getNode i nodes)::graphify_inner st index nodes
	in
	graphify_inner str 0 [];;
