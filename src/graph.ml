(* Library to represent elements of a graph *)

type nodeClass = NodeClass of (char);;
type node = Node of (int * nodeClass);;
type edge =
	Basic of (node * node)
	| Composite of (node * edge) ;;

type adjacency = Adjacency of (node * node list);;

type adjacencyList = Graph of (adjacency list);;

type edgeList = EdgeList of (edge list);;


(*
  Handy function to check if an AdjacencyList conforms to the required format
*)
let rec validate graph =
	let rec node_exists graph node =
		match graph with
		Graph([]) -> false
		| Graph(a::adj_l) ->
			(
			match a with
			Adjacency(n,_) when n = node -> true
			| _ -> let g' = Graph(adj_l) in node_exists g' node
			)
	in
	match graph with
	Graph([]) -> true
	| Graph(a::adj_l) ->
		(
		match a with
		Adjacency(_,[]) -> let g = Graph(adj_l) in validate g
		| Adjacency(x,n::lst) -> let g = Graph(Adjacency(x,lst)::adj_l)
				in
				(node_exists graph n)
		                && (validate g)
		);;

(*
  Function to generate the linear array representation of the graph
*)
let rec encode graph =
	let rec encode_inner graph node =
		let rec getNeighbours graph node =
			(* Return list of all neighbours *)
			match graph with
			[] -> raise Not_found
			| n::g -> (
				   match n with
				   Adjacency(nd, nbrs) when nd = node -> nbrs
				   | _ -> getNeighbours g node
				  )
		in
		let neighbours = getNeighbours graph node
		in
		match neighbours with
		[] -> [node] (* leaf node *)
		| nbr::nbr_list -> (
				    (* Avoid redundant 'node' at the end of its traversal *)
				    match nbr_list with
				    [] -> node::(encode_inner graph nbr)
				    | _ -> (
				    	  (* Return graph with first neighbour of node removed *)
				    	  let rec rm_nbr graph' nd =
				    	  	match graph' with
					  	[] -> raise Not_found
					  	| Adjacency(n, nb::nb_list)::g' when n = nd -> Adjacency(n, nb_list)::g'
					  	| _::g' -> rm_nbr g' nd
				    	in
					(* Tack on the node at the beginning of the sub-traversal to enable reconstruction *)
				    	(node::(encode_inner graph nbr))@(encode_inner (rm_nbr graph node) node)
					)
				  )
	in
	match graph with Graph(g) -> (match g with 
				      Adjacency(first_node,_)::_ -> encode_inner g first_node
				      | [] -> raise Not_found
				     )


(*
  Function to rebuild the adjacency list representation of an encoded graph
*)
let reconstruct enc =
	let rec build_graph enc prev =
		(* Build an empty adjacency list *)
		match enc with
		[] -> raise Not_found
		| [last] -> [Adjacency(last, [])]
		| node::rest -> (
				(* Check if repeated node *)
				match node with
				Node(n,_) when n < prev -> build_graph rest prev
				| Node(n,_) ->
					Adjacency(node,[])::build_graph rest n
				)
	and
	populate_graph graph enc prev =
		match enc with
		[] -> graph
		| node::rest -> (
				let rec alter_graph g nd nbr =
					(* Add nbr as a neighbour of nd *)
					match g with
					[] -> raise Not_found
					| Adjacency(n,nbrs)::g' when n = nd -> (
					 	let node = Adjacency(nd,nbrs@[nbr])
						in node::g' )
					| node::g' -> node :: alter_graph g' nd nbr
				in
				match node with
				n when n < prev -> populate_graph graph rest node
				| _ ->
				  populate_graph (alter_graph graph prev node) rest node
				)
	in
	match enc with first::rest ->
	Graph(populate_graph (build_graph enc (-1)) rest first)
	| [] -> raise Not_found
